import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const STORAGE_NAME = '@oauth2';

export default class Model {
  getStorageAsync() {
    return AsyncStorage.getItem(STORAGE_NAME);
  }

  clearStorageAsync() {
    return AsyncStorage.removeItem(STORAGE_NAME);
  }

  saveStorageAsync(token) {
    return AsyncStorage.setItem(STORAGE_NAME, JSON.stringify({
      [this._keys.access_token]: token[this._keys.access_token],
      [this._keys.access_token_expires_at]: token[this._keys.access_token_expires_at],
      [this._keys.refresh_token]: token[this._keys.refresh_token],
      [this._keys.refresh_token_expires_at]: token[this._keys.refresh_token_expires_at],
      [this._keys.user]: token[this._keys.user],
      [this._keys.client]: token[this._keys.client]
    }));
  }

  _expired(expires_at) {
    return moment().format('YYYY-MM-DD HH:mm:ss') > moment(expires_at).format('YYYY-MM-DD HH:mm:ss');
  }

  async _defaultCode() {
    return {
      [this._keys.code]: null,
      [this._keys.user]: null,
      [this._keys.client]: null
    };
  }

  async _defaultToken() {
    return {
      [this._keys.access_token]: null,
      [this._keys.access_token_expires_at]: null,
      [this._keys.refresh_token]: null,
      [this._keys.refresh_token_expires_at]: null,
      [this._keys.user]: null,
      [this._keys.client]: null
    };
  }

  async _validate() {
    const token = await this.getTokenAsync();

    if(
      token[this._keys.access_token]
      && token[this._keys.access_token_expires_at]
      && token[this._keys.refresh_token]
      && token[this._keys.refresh_token_expires_at]
      && !this._expired(token[this._keys.refresh_token_expires_at])
    ) return token;

    return null;
  }

  async _authorizationCode(args) {
    const token = await this._validate();

    if(token) return token;

    const code = await this.createCode(...args);

    if(!code) return;

    const newtoken = await this.createCodeToken(code[this._keys.code]);

    if(!newtoken) return;

    await this.saveStorageAsync(newtoken);

    return newtoken;
  }

  async _password(args) {
    const token = await this._validate();

    if(token) return token;

    const newtoken = await this.createPasswordToken(...args);

    if(!newtoken) return;

    await this.saveStorageAsync(newtoken);

    return newtoken;
  }
}
