import Model from './Model';

export default class oAuth2 extends Model {
  constructor() {
    super();
  }

  initialize(model, keys = {}) {
    this._model = model;

    this._keys = {
      code: 'code',
      access_token: 'access_token',
      access_token_expires_at: 'access_token_expires_at',
      refresh_token: 'refresh_token',
      refresh_token_expires_at: 'refresh_token_expires_at',
      user: 'user',
      client: 'client',
      ...keys
    };
  }

  get navigation() {
    return this._model.navigation;
  }

  handleError() {
    if(typeof this._model.handleError === 'function')
      return this._model.handleError.call(this, ...arguments);
  }

  createCode() {
    return typeof this._model.createCode === 'function'
      ? this._model.createCode.call(this, ...arguments)
      : this._defaultCode();
  }

  createCodeToken() {
    return typeof this._model.createCodeToken === 'function'
      ? this._model.createCodeToken.call(this, ...arguments)
      : this._defaultToken();
  }

  createPasswordToken() {
    return typeof this._model.createPasswordToken === 'function'
      ? this._model.createPasswordToken.call(this, ...arguments)
      : this._defaultToken();
  }

  refreshToken() {
    return typeof this._model.refreshToken === 'function'
      ? this._model.refreshToken.call(this, ...arguments)
      : this._defaultToken();
  }

  authorizeAsync() {
    const args = Array.from(arguments);

    const flow = args[0];

    args.splice(0, 1);

    switch(flow) {
      case 'authorization_code':
        return this._authorizationCode(args);
      case 'password':
        return this._password(args);
      default:
        throw new Error(`Invalid argument '${flow}'`);
    }
  }

  async getTokenAsync(force = false) {
    const oauth2 = await this.getStorageAsync();

    const token = oauth2
      ? JSON.parse(oauth2)
      : await this._defaultToken();

    if(
      !force
      && token[this._keys.access_token]
      && token[this._keys.access_token_expires_at]
      && !this._expired(token[this._keys.access_token_expires_at])
    ) return token;

    if(
      token[this._keys.refresh_token]
      && token[this._keys.refresh_token_expires_at]
      && !this._expired(token[this._keys.refresh_token_expires_at])
    ) {
      const newtoken = await this.refreshToken(token[this._keys.refresh_token]);

      if(newtoken) {
        await this.saveStorageAsync(newtoken);

        return newtoken;
      }
    }

    return this._defaultToken();
  }

  async signoutAsync() {
    if(typeof this._model.signoutAsync === 'function')
      await this._model.signoutAsync.call(this, ...arguments);

    return this.clearStorageAsync();
  }
}
